//1.Sel·leccionar totes les imatges a la pàgina; registrar en la consola l'atribut alt de cada imagen.
$.each($("img"), function (indexInArray, valueOfElement) { 
    console.log($(valueOfElement).attr("alt"));
    // console.log("aaa");
});

//2.Sel·leccionar l'element input, després anar al formulari i afegeix-li una classe.
$("form > input").addClass("class1");

//3.Sel·leccionar el ítem que té la classe current dins de la llista #myList i el·liminar aquesta classe a l'element; després afegir la classe current al següent ítem de la llista.
$('.current').removeClass("current").next().addClass("current");

//4. Sel·leccionar l'element select dins de #specials; després anar cap al botó submit.
//$("select").offset().$(":submit");

//5.Sel·leccionar el primer ítem de la llista en l'element #slideshow; afegir-li la classe current al mateix i  després afegir la classe disabled als elements germans.
$("ul#slideshow li:eq(0)").addClass("current");
$("ul#slideshow li:gt(0)").addClass("disabled");

//6.Afegir 5 nous ítems al final de la llista desordenada #myList. Ajuda: for (var i = 0; i<5; i++) { ... }
for (let i = 0; i<5; i++) {
    $("#myList").append('<li><a href="#">New list item</a></li>');
}

//7.Remoure els ítems impars de la llista
$("#myList li:even").remove();

//8.Afegir un altre element h2 i un altre paràgref al últim div.module.
$("div.module").last().append('<h2>New Title</h2><p>More words</p>');

//9.Afegir una altre opció a l' element select; donar-li a l'opció afegida el valor Wednesday.
$("select[name = day]").append('<option value="wednesday">Wednesday</option>');

//10.Afegir un nou div.module a la pàgina després del últim; després afegir una còpia d'una de les imatges existents dins del nou div
$('<div id="module"></div>').insertAfter("div.module").last();
$.each($("img"), function (indexInArray, valueOfElement) { 
    $("div.module").last().append(valueOfElement);
});
